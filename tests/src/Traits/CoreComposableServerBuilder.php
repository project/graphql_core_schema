<?php

namespace Drupal\Tests\graphql_core_schema\Traits;

use Drupal\graphql\Entity\Server;
use Drupal\graphql\Entity\ServerInterface;

/**
 * Helper to create a server.
 */
class CoreComposableServerBuilder {

  /**
   * The enabled entity types.
   */
  protected array $enabledEntityTypes;

  /**
   * The enabled extensions.
   */
  protected array $extensions;

  /**
   * Wheter value fields are enabled.
   */
  protected bool $generateValueFields;

  /**
   * The enabled entity base fields.
   */
  protected array $entityBaseFields;

  /**
   * The enabled entity fields.
   */
  protected array $fields;

  /**
   * The enabled entity bundles.
   */
  protected array $bundles = [];

  /**
   * Constructor.
   */
  public function __construct() {
    $this->enabledEntityTypes = [];
    $this->extensions = [];
    $this->entityBaseFields = [];
    $this->fields = [];
    $this->generateValueFields = FALSE;
  }

  /**
   * Enable value fields.
   *
   * @return static
   */
  public function enableValueFields(): static {
    $this->generateValueFields = TRUE;
    return $this;
  }

  /**
   * Enable extensions.
   *
   * @param string $extension
   *   The extension to enable.
   *
   * @return static
   */
  public function enableExtension(string $extension): static {
    $this->extensions[] = $extension;
    return $this;
  }

  /**
   * Enable base entity field.
   *
   * @param string $field
   *   The base entity field.
   *
   * @return static
   */
  public function enableBaseEntityField(string $field): static {
    $this->entityBaseFields[] = $field;
    return $this;
  }

  /**
   * Enable entity type.
   *
   * @param string $entityType
   *   The entity type to enable.
   * @param string[] $fields
   *   The fields of the entity type to enable.
   * @param string[] $bundles
   *   The bundles of the entity type to enable.
   *
   * @return static
   */
  public function enableEntityType(string $entityType, array $fields = [], array $bundles = []): static {
    $this->enabledEntityTypes[] = $entityType;
    $this->fields[$entityType] = array_combine($fields, $fields);
    foreach ($bundles as $bundle) {
      $this->bundles[$entityType][$bundle] = ['enabled' => TRUE];
    }
    return $this;
  }

  /**
   * Sets the bundle config for a given entity type and bundle on the server.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param array $config
   *   The bundle configuration.
   *
   * @return $this
   *   The server builder, for chaining.
   */
  public function setBundleConfig(string $entityType, string $bundle, array $config): static {
    $this->bundles[$entityType][$bundle] = $config;
    return $this;
  }

  /**
   * Create a server.
   *
   * @return ServerInterface
   *   The server
   */
  public function createServer(): ServerInterface {
    $server = Server::create([
      'schema' => 'core_composable',
      'name' => 'test',
      'endpoint' => '/graphql',
      'caching' => FALSE,
      'schema_configuration' => [
        'core_composable' => [
          'generate_value_fields' => $this->generateValueFields,
          'bundles' => $this->bundles,
          'fields' => $this->fields,
          'extensions' => array_combine($this->extensions, $this->extensions),
          'entity_base_fields' => [
            'fields' => array_combine($this->entityBaseFields, $this->entityBaseFields),
          ],
          'enabled_entity_types' => array_fill_keys($this->enabledEntityTypes, 1),
        ],
      ],
    ]);
    $server->save();
    return $server;
  }

}
