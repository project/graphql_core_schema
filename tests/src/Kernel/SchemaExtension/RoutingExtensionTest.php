<?php

declare(strict_types=1);

namespace Drupal\Tests\graphql_core_schema\Kernel\SchemaExtension;

use Drupal\graphql\Entity\ServerInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\graphql_core_schema\Kernel\CoreComposableKernelTestBase;
use GraphQL\Server\OperationParams;

/**
 * Tests the routing extension.
 *
 * @group graphql_core_schema
 */
class RoutingExtensionTest extends CoreComposableKernelTestBase {

  /**
   * The GraphQL server.
   */
  protected ServerInterface $server;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    NodeType::create(['type' => 'article'])->save();

    $node = Node::create([
      'type' => 'article',
      'title' => 'Test',
    ]);
    $node->save();

    $this->server = $this
      ->getCoreComposableServerBuilder()
      ->enableValueFields()
      ->enableExtension('routing')
      ->enableEntityType('node')
      ->createServer();

    $this->setUpCurrentUser(['uid' => 1]);
  }

  /**
   * Test the Url type resolvers.
   */
  public function testUrlTypeResolver(): void {
    $this->assertUrlType('/en/node/1', 'EntityCanonicalUrl');
    $this->assertUrlType('/en/node/1/edit', 'DefaultEntityUrl');
    $this->assertUrlType('/en/admin/content', 'DefaultInternalUrl');
  }

  /**
   * For security reasons we should not route external URLs.
   */
  public function testExternalUrlNotRouted(): void {
    $result = $this->getQueryResult('https://example.com');
    $data = $result['route'];
    $this->assertNull($data);
  }

  /**
   * Assert the resolved Url type.
   *
   * @param string $path
   *   The path.
   * @param string $type
   *   The expected GraphQL type.
   */
  protected function assertUrlType(string $path, string $type) {
    $result = $this->getQueryResult($path);
    $data = $result['route'];
    $this->assertEquals($type, $data['__typename']);
  }

  /**
   * Returns the result of a route query.
   *
   * @param string $path
   *   The path to query.
   *
   * @return array
   *   The resulting route.
   */
  protected function getQueryResult(string $path): array {
    $query = <<<GQL
      query route(\$path: String!) {
        route(path: \$path) {
          __typename
        }
      }
    GQL;
    $params = OperationParams::create([
      'query' => $query,
      'variables' => [
        'path' => $path,
      ],
    ]);
    $result = $this->server->executeOperation($params);

    return $result->data;
  }

}
