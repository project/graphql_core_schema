<?php

declare(strict_types=1);

namespace Drupal\graphql_core_schema\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql_core_schema\CoreComposableConfig;

/**
 * Allows to consult the core composable configuration in schema extensions.
 */
trait CoreComposableConfigurableExtensionTrait {

  /**
   * Returns the configuration of the core composable endpoint.
   *
   * @return \Drupal\graphql_core_schema\CoreComposableConfig
   *   The core composable configuration.
   */
  protected function getCoreComposableConfig(): CoreComposableConfig {
    assert($this->configuration['core_composable'] instanceof CoreComposableConfig);
    return $this->configuration['core_composable'];
  }

}
